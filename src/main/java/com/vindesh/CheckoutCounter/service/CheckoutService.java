package com.vindesh.CheckoutCounter.service;

import com.vindesh.CheckoutCounter.model.Item;

import java.util.List;
import java.util.Map;

public interface CheckoutService {

    /**
     * Service which prepares Bill for the given cart
     * @param itemList
     * @return
     */
    Map<String,Object> prepareBillAndCheckout(List<Item> itemList);
}
