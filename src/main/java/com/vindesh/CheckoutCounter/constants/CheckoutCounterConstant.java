package com.vindesh.CheckoutCounter.constants;

public class CheckoutCounterConstant {

    public static final String EXCEPTION_MESSAGE = "Something Went Wrong";

    public static final String SERVICE_RUNNING_MESSAGE = "Service is up and running";

    public static final String ITEMWISE_BILL_JSON_KEY = "ItemWiseBill";

    public static final String TOTAL_PRICE_JSON_KEY = "TotalPrice";

    public static final String TOTAL_TAX_JSON_KEY = "TotalTax";
}
