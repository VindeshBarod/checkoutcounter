package com.vindesh.CheckoutCounter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vindesh.CheckoutCounter.model.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static com.vindesh.CheckoutCounter.model.Category.A;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CheckoutControllerTest {

    private MockMvc mvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test_prepareBillAndCheckout() throws Exception{
        String uri = "/checkout";
        List<Item> itemList = new ArrayList<>();
        Item item = new Item();
        item.setName("Sugar");
        item.setPrice(20);
        item.setQuantity(2);
        item.setCategory(A);
        itemList.add(item);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(new ObjectMapper().writeValueAsString(itemList))).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("{\"TotalTax\":4.0," +
                "\"TotalPrice\":40.0," +
                "\"ItemWiseBill\":" +
                "[{\"productName\":\"Sugar\"," +
                "\"price\":20.0," +
                "\"quantity\":2," +
                "\"tax\":4.0}]}",content);
    }

}
