package com.vindesh.CheckoutCounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.vindesh.CheckoutCounter")
public class CheckoutCounterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckoutCounterApplication.class, args);
	}

}
