package com.vindesh.CheckoutCounter.service.impl;

import com.vindesh.CheckoutCounter.helper.TaxCalculator;
import com.vindesh.CheckoutCounter.model.BillingWrapper;
import com.vindesh.CheckoutCounter.model.Item;
import com.vindesh.CheckoutCounter.service.CheckoutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vindesh.CheckoutCounter.constants.CheckoutCounterConstant.*;

@Service
public class CheckoutServiceImpl implements CheckoutService {

    private static final Logger logger = LoggerFactory.getLogger(CheckoutServiceImpl.class);

    @Autowired
    private TaxCalculator taxCalculator;

    @Override
    public Map<String, Object> prepareBillAndCheckout(List<Item> itemList) {
        logger.info("Going to prepare bill for items");
        return prepareBillForEachItem(itemList);
    }

    private Map<String, Object> prepareBillForEachItem(List<Item> itemList) {
        logger.info("Start Preparing bill for each Item");
        Map<String, Object> billMap = new HashMap<>();
        List<BillingWrapper> billingWrapperList = new ArrayList<>();
        double totalPrice = 0; double totalTax = 0;
        BillingWrapper billingWrapper;
        for (Item item : itemList) {
            billingWrapper = getBillForItem(item);
            billingWrapperList.add(billingWrapper);
            totalPrice += (billingWrapper.getPrice() * billingWrapper.getQuantity());
            totalTax += (billingWrapper.getTax());
        }
        billMap.put(ITEMWISE_BILL_JSON_KEY, billingWrapperList);
        billMap.put(TOTAL_PRICE_JSON_KEY, totalPrice);
        billMap.put(TOTAL_TAX_JSON_KEY, totalTax);
        logger.info("Bill got prepared for each Item");
        return billMap;
    }

    private BillingWrapper getBillForItem(Item item) {
        BillingWrapper billingWrapper = new BillingWrapper();
        double tax = taxCalculator.calculateTax(item);
        billingWrapper.setTax(tax);
        billingWrapper.setPrice(item.getPrice());
        billingWrapper.setProductName(item.getName());
        billingWrapper.setQuantity(item.getQuantity());
        return billingWrapper;
    }
}
