package com.vindesh.CheckoutCounter;

import com.vindesh.CheckoutCounter.helper.TaxCalculator;
import com.vindesh.CheckoutCounter.model.Item;
import com.vindesh.CheckoutCounter.service.impl.CheckoutServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.vindesh.CheckoutCounter.constants.CheckoutCounterConstant.TOTAL_PRICE_JSON_KEY;
import static com.vindesh.CheckoutCounter.constants.CheckoutCounterConstant.TOTAL_TAX_JSON_KEY;
import static com.vindesh.CheckoutCounter.model.Category.A;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutServiceImplTest {

    @InjectMocks
    private CheckoutServiceImpl checkoutService;

    @Mock
    private TaxCalculator taxCalculator;

    private List<Item> itemList = new ArrayList<>();

    @Before
    public void setUp() {
        Item item = new Item();
        item.setName("Sugar");
        item.setPrice(20);
        item.setQuantity(2);
        itemList.add(item);
    }

    @Test
    public void test_prepareBillAndCheckout_positive() {
        Mockito.when(taxCalculator.calculateTax(Mockito.any(Item.class))).thenReturn(4.0);
        itemList.get(0).setCategory(A);
        Map<String, Object> map = checkoutService.prepareBillAndCheckout(itemList);
        assertNotNull(map);
        assertEquals(3,map.size());
        assertEquals(40.0,map.get(TOTAL_PRICE_JSON_KEY));
        assertEquals(4.0, map.get(TOTAL_TAX_JSON_KEY));
    }
}
