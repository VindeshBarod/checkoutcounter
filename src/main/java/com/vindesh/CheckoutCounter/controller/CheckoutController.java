package com.vindesh.CheckoutCounter.controller;

import com.vindesh.CheckoutCounter.exception.BillGenerationException;
import com.vindesh.CheckoutCounter.model.Item;
import com.vindesh.CheckoutCounter.service.CheckoutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static com.vindesh.CheckoutCounter.constants.CheckoutCounterConstant.EXCEPTION_MESSAGE;
import static com.vindesh.CheckoutCounter.constants.CheckoutCounterConstant.SERVICE_RUNNING_MESSAGE;

@RestController
public class CheckoutController {

    private static final Logger logger = LoggerFactory.getLogger(CheckoutController.class);

    @Autowired
    private CheckoutService checkoutService;

    /**
     * API to check if the service is running or not
     * @return
     */
    @GetMapping("/isServiceUp")
    public String isServiceUpAndRunning() {
        return SERVICE_RUNNING_MESSAGE;
    }

    /**
     * API to generate Bill for given Cart Items
     * @param itemList
     * @return
     * @throws BillGenerationException
     */
    @PostMapping("/checkout")
    public Map<String,Object> prepareBillAndCheckout(@RequestBody List<Item> itemList) throws BillGenerationException {
        logger.info("Going to prepare bill for {} items", itemList.size());
        try {
            return checkoutService.prepareBillAndCheckout(itemList);
        } catch(Exception e) {
            logger.error("Error while generating bill : ",e);
            throw new BillGenerationException(EXCEPTION_MESSAGE);
        }
    }
}
