package com.vindesh.CheckoutCounter.helper;

import com.vindesh.CheckoutCounter.model.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TaxCalculator {

    private static final Logger logger = LoggerFactory.getLogger(TaxCalculator.class);

    @Value("${category.A.tax}")
    private int taxForCategoryA;

    @Value("${category.B.tax}")
    private int taxForCategoryB;

    @Value("${category.C.tax}")
    private int taxForCategoryC;

    public double calculateTax(Item item) {
        logger.debug("Calculating tax for Item : {}", item);
        switch(item.getCategory()) {
            case A:
                return item.getPrice() * (taxForCategoryA/100.00) * item.getQuantity();
            case B:
                return item.getPrice() * (taxForCategoryB/100.00) * item.getQuantity();
            case C:
                return item.getPrice() * (taxForCategoryC/100.00) * item.getQuantity();
            default:
                return 0;
        }
    }
}
