package com.vindesh.CheckoutCounter;

import com.vindesh.CheckoutCounter.helper.TaxCalculator;
import com.vindesh.CheckoutCounter.model.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static com.vindesh.CheckoutCounter.model.Category.A;
import static com.vindesh.CheckoutCounter.model.Category.B;
import static com.vindesh.CheckoutCounter.model.Category.C;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TaxCalculatorTest {

    @InjectMocks
    private TaxCalculator taxCalculator;

    private Item item = new Item();

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(taxCalculator,"taxForCategoryA",10);
        ReflectionTestUtils.setField(taxCalculator,"taxForCategoryB",20);
        ReflectionTestUtils.setField(taxCalculator,"taxForCategoryC",0);
        item.setQuantity(2);
        item.setPrice(10);
    }

    @Test
    public void test_calculateTax_For_Category_A() {
        item.setCategory(A);
        double tax = taxCalculator.calculateTax(item);
        assertEquals(2.0,tax,0.0);
    }

    @Test
    public void test_calculateTax_For_Category_B() {
        item.setCategory(B);
        double tax = taxCalculator.calculateTax(item);
        assertEquals(4.0,tax,0.0);
    }

    @Test
    public void test_calculateTax_For_Category_C() {
        item.setCategory(C);
        double tax = taxCalculator.calculateTax(item);
        assertEquals(0.0,tax,0.0);
    }
}
