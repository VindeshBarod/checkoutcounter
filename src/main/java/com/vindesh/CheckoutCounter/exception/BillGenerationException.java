package com.vindesh.CheckoutCounter.exception;

public class BillGenerationException extends Exception {

    public BillGenerationException(String message) {
       super(message);
    }

    public String getMessage() {
        return "Something went wrong while bill generation";
    }

}
